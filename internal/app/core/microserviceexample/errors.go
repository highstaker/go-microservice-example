package microserviceexample

import "errors"

var (
	ErrRandomNumber     = errors.New("even doing uselessness causes errors once in a while")
	ErrExemplarNotFound = errors.New("exemplar not found")
)
