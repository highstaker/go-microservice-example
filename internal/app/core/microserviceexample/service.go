package microserviceexample

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample/dto"
	utilsInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
)

type MicroserviceExampleService struct {
	logger utilsInterfaces.Logger

	mongoRepo mongoRepo
}

func NewService(
	logger utilsInterfaces.Logger,
	mongoRepo mongoRepo,
) *MicroserviceExampleService {
	return &MicroserviceExampleService{
		logger:    logger,
		mongoRepo: mongoRepo,
	}
}

func (s *MicroserviceExampleService) DoNothing() {
	s.logger.Info("Started doing nothing")
	s.logger.Info("Finished doing nothing")
}

func (s *MicroserviceExampleService) PlayWithRandomNumber(n int64) (int64, error) {
	s.logger.Info("Started playing with random number")
	time.Sleep(1 * time.Second)

	randomNumber := rand.Int63n(n)
	if randomNumber == 0 {
		s.logger.Errorf("whoops, random error. Number: %v", randomNumber)
		return 0, ErrRandomNumber
	}

	s.logger.Infof("Finished playing with random number. Number: %v", randomNumber)

	return randomNumber, nil
}

func (s *MicroserviceExampleService) GetExemplar(ctx context.Context, id string) (dto.Exemplar, error) {
	result, err := s.mongoRepo.GetExemplarByID(ctx, id)
	if err != nil {
		s.logger.WithErr(err).Errorf("could not get exemplar by ID")
		return dto.Exemplar{}, err
	}

	return result, nil
}

func (s *MicroserviceExampleService) GetExemplarCount(ctx context.Context) (int64, error) {
	result, err := s.mongoRepo.GetExemplarCount(ctx)
	if err != nil {
		s.logger.WithErr(err).Errorf("could not get exemplar count")
		return 0, err
	}

	return result, nil
}

func (s *MicroserviceExampleService) InsertExemplars(ctx context.Context, exemplars []dto.Exemplar) (int64, error) {
	result, err := s.mongoRepo.InsertExemplars(ctx, exemplars)
	if err != nil {
		s.logger.WithErr(err).Errorf("could not insert exemplars")
		return result, err
	}

	return result, nil
}
