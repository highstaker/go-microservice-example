package microserviceexample

import (
	"context"

	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample/dto"
)

type mongoRepo interface {
	GetExemplarByID(ctx context.Context, id string) (dto.Exemplar, error)
	GetExemplarCount(ctx context.Context) (int64, error)
	InsertExemplars(ctx context.Context, exemplars []dto.Exemplar) (int64, error)
}
