package grpc

import (
	"context"
	"errors"

	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample"
	v1 "gitlab.com/highstaker/go-microservice-example/pkg/api/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ExampleGRPC struct {
	v1.UnimplementedGoMicroserviceExampleServer

	service *microserviceexample.MicroserviceExampleService
}

func New(service *microserviceexample.MicroserviceExampleService) *ExampleGRPC {
	return &ExampleGRPC{
		service: service,
	}
}

func (srv *ExampleGRPC) RunExample(ctx context.Context, req *v1.RunExampleRequest) (*v1.RunExampleResponse, error) {
	srv.service.DoNothing()

	return &v1.RunExampleResponse{}, nil
}

func (srv *ExampleGRPC) GetRandomNumber(ctx context.Context, req *v1.GetRandomNumberRequest) (*v1.GetRandomNumberResponse, error) {
	var n int64 = 10
	if req.GetMaxNumber() > 0 {
		n = req.GetMaxNumber()
	}

	result, err := srv.service.PlayWithRandomNumber(n)
	if err != nil {
		return nil, status.Errorf(codes.Aborted,
			"somehow you've managed to get an error from a useless random number: %v",
			err.Error(),
		)
	}

	return &v1.GetRandomNumberResponse{
		ResultingNumber: result,
	}, nil
}

func (srv *ExampleGRPC) GetExemplar(ctx context.Context, req *v1.GetExemplarRequest) (*v1.GetExemplarResponse, error) {
	exemplar, err := srv.service.GetExemplar(ctx, req.GetId())
	if err != nil {
		if errors.Is(err, microserviceexample.ErrExemplarNotFound) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal error")
	}

	pbExemplar := toProtoExemplar(exemplar)
	return &v1.GetExemplarResponse{Exemplar: pbExemplar}, nil
}

func (srv *ExampleGRPC) GetExemplarCount(ctx context.Context, req *v1.GetExemplarCountRequest) (*v1.GetExemplarCountResponse, error) {
	count, err := srv.service.GetExemplarCount(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "internal error")
	}

	return &v1.GetExemplarCountResponse{Count: count}, nil
}

func (srv *ExampleGRPC) InsertExemplars(ctx context.Context, req *v1.InsertExemplarsRequest) (*v1.InsertExemplarsResponse, error) {
	exemplars := fromProtoExemplars(req.GetExemplars())

	count, err := srv.service.InsertExemplars(ctx, exemplars)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "internal error")
	}

	return &v1.InsertExemplarsResponse{Count: count}, nil
}

//TODO:
// func (srv *ExampleGRPC) GetExemplarByGroupRole(ctx context.Context, req *v1.GetExemplarRequest) (*v1.GetExemplarResponse, error) {
// //TODO:
// }
