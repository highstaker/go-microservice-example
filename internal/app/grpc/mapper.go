package grpc

import (
	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample/dto"
	v1 "gitlab.com/highstaker/go-microservice-example/pkg/api/v1"
)

func toProtoExemplar(in dto.Exemplar) *v1.Exemplar {
	return &v1.Exemplar{
		Id:      in.ID,
		GroupId: in.GroupID,
	}
}

func fromProtoExemplar(in *v1.Exemplar) dto.Exemplar {
	return dto.Exemplar{
		ID:      in.GetId(),
		GroupID: in.GetGroupId(),
	}
}

func fromProtoExemplars(in []*v1.Exemplar) []dto.Exemplar {
	result := make([]dto.Exemplar, 0, len(in))
	for _, el := range in {
		result = append(result, fromProtoExemplar(el))
	}

	return result
}
