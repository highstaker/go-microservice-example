package mongodb

import "gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample/dto"

func fromModelExemplar(in Exemplar) dto.Exemplar {
	return dto.Exemplar{
		ID:      in.ID,
		GroupID: in.GroupID,
	}
}
