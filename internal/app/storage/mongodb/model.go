package mongodb

type Exemplar struct {
	ID      string `bson:"id"`
	GroupID string `bson:"group_id"`
}
