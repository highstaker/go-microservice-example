package mongodb

import (
	"context"

	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample"
	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample/dto"
	utilsInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
)

const (
	collectionExemplar = "exemplar"
)

type MongoRepo struct {
	client utilsInterfaces.MongoClient
}

func New(client utilsInterfaces.MongoClient) *MongoRepo {
	return &MongoRepo{
		client: client,
	}
}

func (repo *MongoRepo) GetExemplarByID(ctx context.Context, id string) (dto.Exemplar, error) {
	result := Exemplar{}
	err := repo.client.GetOne(ctx, collectionExemplar, &result, utilsInterfaces.MongoClientFilter{Key: "id", Value: id})
	if err != nil {
		if repo.client.IsErrNotFound(err) {
			return dto.Exemplar{}, microserviceexample.ErrExemplarNotFound
		}
		return dto.Exemplar{}, err
	}

	return fromModelExemplar(result), nil
}

func (repo *MongoRepo) GetExemplarCount(ctx context.Context) (int64, error) {
	count, err := repo.client.GetCount(ctx, collectionExemplar)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (repo *MongoRepo) InsertExemplars(ctx context.Context, exemplars []dto.Exemplar) (int64, error) {

	// a canonical way, can't do anything better.
	// https://github.com/golang/go/wiki/InterfaceSlice#what-can-i-do-instead
	iExemplars := make([]interface{}, 0, len(exemplars))
	for _, el := range exemplars {
		iExemplars = append(iExemplars, el)
	}

	count, err := repo.client.Insert(ctx, collectionExemplar, iExemplars)
	if err != nil {
		return count, err
	}

	return count, nil
}
