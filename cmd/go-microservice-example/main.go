package main

import (
	"log"

	"gitlab.com/highstaker/go-microservice-example/internal/app/core/microserviceexample"
	grpcServer "gitlab.com/highstaker/go-microservice-example/internal/app/grpc"
	"gitlab.com/highstaker/go-microservice-example/internal/app/storage/mongodb"
	v1 "gitlab.com/highstaker/go-microservice-example/pkg/api/v1"
	"gitlab.com/highstaker/go-microservice-utils/pkg/application"
	utilsServerGrpc "gitlab.com/highstaker/go-microservice-utils/pkg/servers/grpc_server"
)

func main() {
	//TODO: custom configs

	app, err := application.New()
	if err != nil {
		log.Fatalf("could not create new application: %s", err.Error())
	}

	createServices(app)

	if err := app.Run(); err != nil {
		log.Fatalf("application exited with error: %s", err.Error())
	}
}

func createServices(app *application.Application) {
	mongoRepo := mongodb.New(app.GetMongoClient())

	svc := microserviceexample.NewService(app.GetLogger(), mongoRepo)

	registerGRPC(app, svc)
}

func registerGRPC(app *application.Application, svc *microserviceexample.MicroserviceExampleService) {

	srv := grpcServer.New(svc)

	//TODO: make disableable via config
	//if app.GetConfig().
	if grpcServer, ok := app.GetServerByName(utilsServerGrpc.ServerNameGRPC).(utilsServerGrpc.ServerGetter); ok {
		v1.RegisterGoMicroserviceExampleServer(grpcServer.GetServer(), srv)
	} else {
		app.GetLogger().Fatalf("GRPC server is not ServiceRegistrar")
	}

}
