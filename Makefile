#!make

install-protobuf:
	sudo apt-get install -y wget curl gcc protobuf-compiler
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

# create or update files with protobuf-generated code
generate:
	protoc -I . -I ./api --go_out ./pkg/ --go_opt paths=source_relative --go-grpc_out ./pkg/ --go-grpc_opt paths=source_relative ./api/v1/*.proto

build:
	go build cmd/go-microservice-example/main.go

check-compile:
	go vet ./...

mongo:
	docker run --rm -it --name goexamplemongo -e MONGO_INITDB_ROOT_USERNAME=exampleuser -e MONGO_INITDB_ROOT_PASSWORD=examplepass -e MONGO_INITDB_DATABASE=exampledb -p 27017:27017 mongo:6
